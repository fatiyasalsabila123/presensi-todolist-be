package com.PresensiToDoList.PresensiToDoList.Configuration;

import com.PresensiToDoList.PresensiToDoList.JWT.AccessDanied;
import com.PresensiToDoList.PresensiToDoList.JWT.JWTAuthTokenFilter;
import com.PresensiToDoList.PresensiToDoList.JWT.UnautorizationError;
import com.PresensiToDoList.PresensiToDoList.service.UsersDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

//alur kedua pembuatan configuration

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AccessDanied accessDaniedHandler;

    @Autowired
    private UnautorizationError unautorizationError;


    @Autowired
    private UsersDetailServiceImpl usersDetailService;

    private static final String[] AUTH_WHITLISH = {
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            // -- Swagger UI v3 (OpenAPI)
            "/v3/api-docs/** ",
            "/swagger-ui/**",
            "/authentication/**",
            "/users/login",
            "/users/register",
            "/presensi/{Id}",
            "/presensi/all",
            "/presensi/post",
            "/presensi/**",
            "/todolist/all",
            "/todolist/{Id}",
            "/todolist/**",
            "/absen_masuk/all",
            "/absen_masuk/{Id}",
            "/absen_pulang/all",
            "/absen_pulang",
            "/absen_masuk",
            "/absen_pulang/{Id}",
            "/selesai/post",
            "/todolist/selesai/{Id}",
            "/image",
            "/users/**"
    };

    @Bean
    public JWTAuthTokenFilter authTokenFilter() {
        return new JWTAuthTokenFilter();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(usersDetailService).passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unautorizationError).and()
                .exceptionHandling().accessDeniedHandler(accessDaniedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .antMatchers(AUTH_WHITLISH).permitAll()
                .anyRequest().authenticated();
        http.addFilterBefore(authTokenFilter(),
                UsernamePasswordAuthenticationFilter.class);
    }

}
