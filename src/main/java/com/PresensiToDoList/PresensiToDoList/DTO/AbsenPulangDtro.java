package com.PresensiToDoList.PresensiToDoList.DTO;

import java.util.Date;

//alur ke empat pembuat absen pulang
public class AbsenPulangDtro {

    private Long usersId;

    private String status;

    private Date waktu;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getUsersId() {
        return usersId;
    }

    public void setUsersId(Long usersId) {
        this.usersId = usersId;
    }

    public Date getWaktu() {
        return waktu;
    }

    public void setWaktu(Date waktu) {
        this.waktu = waktu;
    }
}
