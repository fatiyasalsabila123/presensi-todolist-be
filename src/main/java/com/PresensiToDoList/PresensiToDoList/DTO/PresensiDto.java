package com.PresensiToDoList.PresensiToDoList.DTO;

//alur ke empat pembuat presensi

public class PresensiDto {

    private Long usersId;

    private String nama;
    private String gender;

    public PresensiDto() {
    }

    public Long getUsersId() {
        return usersId;
    }

    public void setUsersId(Long usersId) {
        this.usersId = usersId;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
