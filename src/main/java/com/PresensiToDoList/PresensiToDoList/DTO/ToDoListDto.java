package com.PresensiToDoList.PresensiToDoList.DTO;

import com.PresensiToDoList.PresensiToDoList.model.Users;

//alur ke empat pembuat todolist
public class ToDoListDto  {

    private String note;

    private Long usersId;

    public ToDoListDto() {
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getUsersId() {
        return usersId;
    }

    public void setUsersId(Long usersId) {
        this.usersId = usersId;
    }

}
