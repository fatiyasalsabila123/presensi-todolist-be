package com.PresensiToDoList.PresensiToDoList.DTO;

import com.PresensiToDoList.PresensiToDoList.enumated.UserSType;

//alur ke sepuluh pembuatan login/register

public class UsersDTO {

    private String email;

    private String password;

    private String username;

    private UserSType role;

//    private String profile;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserSType getRole() {
        return role;
    }

    public void setRole(UserSType role) {
        this.role = role;
    }
}
