package com.PresensiToDoList.PresensiToDoList.JWT;

import com.PresensiToDoList.PresensiToDoList.model.TemporaryToken;
import com.PresensiToDoList.PresensiToDoList.repository.UsersRepository;
import com.PresensiToDoList.PresensiToDoList.service.UsersDetailServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//alur ke tiga pembuatan jwt
//alur ke empat pembuat token

public class JWTAuthTokenFilter extends OncePerRequestFilter {

    @Autowired
    private JWTProvider jwtUtils;

    @Autowired
    private UsersDetailServiceImpl usersDetailService;

    @Autowired
    private UsersRepository usersRepository;

    private static final Logger logger = LoggerFactory.getLogger(JWTAuthTokenFilter.class);

    @Override
protected void doFilterInternal (HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String jwt = parseJwt(request);
            if (jwt != null && jwtUtils.checkingTokenJwt(jwt)) {
                TemporaryToken token = jwtUtils.getSubject(jwt);
                UserDetails userDetails = usersDetailService.loadUserByUsername(usersRepository.findById(token.getUserId()).get().getEmail());


                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }catch (Exception e) {
            logger.error("Cannot set User Authentication: {}", e);
        }
        filterChain.doFilter(request, response);
    }

    private String parseJwt(HttpServletRequest request) {
        String headerAuth = request.getHeader("Authorization");
        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return  headerAuth.substring(7, headerAuth.length());
        }
        return null;
    }
}
