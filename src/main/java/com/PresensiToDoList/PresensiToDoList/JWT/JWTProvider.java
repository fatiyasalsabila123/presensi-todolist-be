package com.PresensiToDoList.PresensiToDoList.JWT;

import com.PresensiToDoList.PresensiToDoList.exception.InternalErrorException;
import com.PresensiToDoList.PresensiToDoList.exception.NotFoundException;
import com.PresensiToDoList.PresensiToDoList.model.TemporaryToken;
import com.PresensiToDoList.PresensiToDoList.model.Users;
import com.PresensiToDoList.PresensiToDoList.service.TemporaryTokenRepository;
import com.PresensiToDoList.PresensiToDoList.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

//alur ke dua pembuatan jwt
//alur ke tiga pembuatan temporary token

@Component
public class JWTProvider {

    private static String seccetkey = "ToDoList";

    private static Integer expired = 900000;

    @Autowired
    public TemporaryTokenRepository temporaryTokenRepository;

    @Autowired
    private UsersRepository usersRepository;

    public String generateToken(UserDetails usersDetails) {
        String token = UUID.randomUUID().toString().replace("_", "");
        Users users = usersRepository.findByEmail(usersDetails.getUsername()).orElseThrow(() -> new NotFoundException("User Not Found Generate Token"));
        var chekingToken = temporaryTokenRepository.findByUserId(users.getId());
        if (chekingToken.isPresent()) temporaryTokenRepository.deleteById(chekingToken.get().getId());
        TemporaryToken temporaryToken = new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setUserId(users.getId());
        temporaryTokenRepository.save(temporaryToken);
        return token;
    };

    public TemporaryToken getSubject (String token) {
        return temporaryTokenRepository.findByToken(token).orElseThrow(() -> new InternalErrorException("Token Error Parse"));
    }

    public boolean checkingTokenJwt(String token) {
        TemporaryToken tokenExits = temporaryTokenRepository.findByToken(token).orElse(null);
        if (tokenExits == null) {
            System.out.println("Token Kosong");
            return false;
        }
        if (tokenExits.getExpiredDate().before(new Date())) {
            System.out.println("Token Expired");
            return false;
        }
        return true;
    }
}
