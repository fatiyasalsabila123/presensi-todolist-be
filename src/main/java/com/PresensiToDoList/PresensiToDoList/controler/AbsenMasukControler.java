package com.PresensiToDoList.PresensiToDoList.controler;

import com.PresensiToDoList.PresensiToDoList.DTO.AbsenMasukDto;
import com.PresensiToDoList.PresensiToDoList.model.AbsenMasuk;
import com.PresensiToDoList.PresensiToDoList.response.CommonResponse;
import com.PresensiToDoList.PresensiToDoList.response.ResponHelper;
import com.PresensiToDoList.PresensiToDoList.service.MasukAbsenService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//alur keenam pembuatan absen masuk

@RestController
@RequestMapping("/absen_masuk")
public class AbsenMasukControler {

    @Autowired
    private MasukAbsenService masukAbsenService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public CommonResponse<List<AbsenMasuk>> getAllAbsenMasuk(@RequestParam (name = "user_id") Long usersId) {
        return ResponHelper.ok(masukAbsenService.getAllAbsenMasuk(usersId));
    }

    @GetMapping("/{Id}")
    public CommonResponse<AbsenMasuk> getAbsenMasuk(@PathVariable("Id") Long Id) {
        return ResponHelper.ok(masukAbsenService.getAbsenMasuk(Id));
    }

    @PostMapping
    public CommonResponse<AbsenMasuk> addAbsenMasuk(@RequestBody AbsenMasukDto absenMasuk) {
        return ResponHelper.ok(masukAbsenService.addAbsenMasuk(absenMasuk));
    }

    @PutMapping("/{Id}")
    public CommonResponse<AbsenMasuk> editAbsenMasauk(@PathVariable("Id") Long Id, @RequestParam("passagerTypeId") @RequestBody AbsenMasukDto absenMasukDto) {
        return ResponHelper.ok(masukAbsenService.editAbsenMasauk(Id,modelMapper.map(absenMasukDto, AbsenMasuk.class)));
    }
}
