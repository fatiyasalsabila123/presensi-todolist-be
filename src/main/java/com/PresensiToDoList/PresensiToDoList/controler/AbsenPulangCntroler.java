package com.PresensiToDoList.PresensiToDoList.controler;

import com.PresensiToDoList.PresensiToDoList.DTO.AbsenPulangDtro;
import com.PresensiToDoList.PresensiToDoList.model.AbsenPulang;
import com.PresensiToDoList.PresensiToDoList.response.CommonResponse;
import com.PresensiToDoList.PresensiToDoList.response.ResponHelper;
import com.PresensiToDoList.PresensiToDoList.service.AbsenPulangService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//alur keenam pembuatan absen pulang

@RestController
@RequestMapping("/absen_pulang")
public class AbsenPulangCntroler {

    @Autowired
    private AbsenPulangService absenPulangService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public CommonResponse<List<AbsenPulang>> getAll(@RequestParam (name = "users_id") Long usersId) {
        return ResponHelper.ok(absenPulangService.getAll(usersId));
    }

    @GetMapping("/{Id}")
    public CommonResponse<AbsenPulang> getAbsenPulang(@PathVariable("Id") Long Id) {
        return ResponHelper.ok(absenPulangService.getAbsenPulang(Id));
    }

    @PostMapping
    public CommonResponse<AbsenPulang> addAbsenPulang(@RequestBody AbsenPulangDtro absenPulangDtro) {
        return ResponHelper.ok(absenPulangService.addAbsenPulang(absenPulangDtro));
    }

    @PutMapping("/{Id}")
    public CommonResponse<AbsenPulang> editAbsenPulang(@PathVariable("Id") Long Id,@RequestBody AbsenPulangDtro absenPulangDtro) {
        return ResponHelper.ok(absenPulangService.editAbsenPulang(Id, modelMapper.map(absenPulangDtro, AbsenPulang.class)));
    }

}
