package com.PresensiToDoList.PresensiToDoList.controler;

import com.PresensiToDoList.PresensiToDoList.DTO.PresensiDto;
import com.PresensiToDoList.PresensiToDoList.DTO.UsersDTO;
import com.PresensiToDoList.PresensiToDoList.model.Presensi;
import com.PresensiToDoList.PresensiToDoList.response.CommonResponse;
import com.PresensiToDoList.PresensiToDoList.response.ResponHelper;
import com.PresensiToDoList.PresensiToDoList.service.PresensitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//alur keenam pembuatan presensi

@RestController
@CrossOrigin(origins = "https://localhost:4000")
@RequestMapping("/presensi")
public class PresensiControler {

    @Autowired
    private PresensitService presensitService;

    @GetMapping
    public CommonResponse<List<Presensi>> getAll(@RequestParam (name = "user_id") Long usersId){
        return ResponHelper.ok(presensitService.getAll(usersId));
    }
    @GetMapping("/{Id}")
    public CommonResponse<Presensi> getPresensi(@PathVariable("Id")Long Id) {
        return ResponHelper.ok(presensitService.getPresensi(Id));
    }

    @PostMapping
    public CommonResponse<Presensi> addPresensi(@RequestBody PresensiDto presensi) {
        return ResponHelper.ok(presensitService.addPresensi(presensi));
    }

    @PutMapping("/{Id}")
    public CommonResponse<Presensi> EditPresensi(@PathVariable("Id") Long Id,@RequestBody Presensi presensi) {
        return ResponHelper.ok(presensitService.EditPresensi(Id, presensi));
    }

    @DeleteMapping("/{Id}")
    public void deletePresesnsiById(@PathVariable("Id") Long Id) {
        presensitService.deletePresensiById(Id);
    }
}
