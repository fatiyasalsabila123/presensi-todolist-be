package com.PresensiToDoList.PresensiToDoList.controler;

import com.PresensiToDoList.PresensiToDoList.DTO.PresensiDto;
import com.PresensiToDoList.PresensiToDoList.DTO.ToDoListDto;
import com.PresensiToDoList.PresensiToDoList.model.Presensi;
import com.PresensiToDoList.PresensiToDoList.model.Todolist;
import com.PresensiToDoList.PresensiToDoList.response.CommonResponse;
import com.PresensiToDoList.PresensiToDoList.response.ResponHelper;
import com.PresensiToDoList.PresensiToDoList.service.TodolistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//alur keenam pembuatan todolist

@RestController
@CrossOrigin(origins = "https://localhost:4000")
@RequestMapping("/todolist")
public class TodolistControler {


    @Autowired
    private TodolistService todolistService;

    @GetMapping
    public CommonResponse<List<Todolist>> getAllTodolist(@RequestParam (name = "user_id") Long usersId){
        return ResponHelper.ok(todolistService.getAllTodolist(usersId));
    }

    @GetMapping("/{Id}")
    public CommonResponse<Todolist> getTodolist(@PathVariable("Id")Long Id) {
        return ResponHelper.ok(todolistService.getTodolist(Id));
    }

    @PostMapping
    public CommonResponse<Todolist> addTodolist(@RequestBody ToDoListDto todolist) {
        return ResponHelper.ok(todolistService.addTodolist(todolist));
    }

    @PutMapping("/selesai/{Id}")
    public CommonResponse<Todolist> checklist(@PathVariable("Id") Long Id) {
        return ResponHelper.ok(todolistService.checklist(Id));
    }

    @PutMapping("/{Id}")
    public CommonResponse<Todolist> EditTodolist(@PathVariable("Id") Long Id, Todolist todolist) {
        return ResponHelper.ok(todolistService.EditTodolist(Id, todolist));
    }

    @DeleteMapping("/{Id}")
    public void deleteTodolist(@PathVariable("Id") Long Id) {
        todolistService.deleteTodolist(Id);
    }

}
