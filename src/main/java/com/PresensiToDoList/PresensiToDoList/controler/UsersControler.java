package com.PresensiToDoList.PresensiToDoList.controler;

import com.PresensiToDoList.PresensiToDoList.DTO.LoginDto;
import com.PresensiToDoList.PresensiToDoList.DTO.UsersDTO;
import com.PresensiToDoList.PresensiToDoList.model.Users;
import com.PresensiToDoList.PresensiToDoList.response.CommonResponse;
import com.PresensiToDoList.PresensiToDoList.response.ResponHelper;
import com.PresensiToDoList.PresensiToDoList.service.UsersService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

//alu ke sebelas pembuatan login/register

@RestController
@CrossOrigin(origins = "http://localhost:4000")
@RequestMapping("/users")
public class UsersControler {

    @Autowired
    private UsersService usersService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/login")
    public CommonResponse<Map<String, Object>> login(@RequestBody LoginDto loginDto) {
        return ResponHelper.ok(usersService.login(loginDto));
    }

    @PostMapping("/register")
    public CommonResponse<Users> addUsers(UsersDTO usersDTO) {
        return ResponHelper.ok(usersService.addUsers(usersDTO));
    }

    @GetMapping("/all")
    public CommonResponse<List<Users>> getAllUsers() {
        return ResponHelper.ok(usersService.getAllUsers());
    }

    @GetMapping("/{Id}")
    public CommonResponse<Users> getUsers(@PathVariable("Id")Long Id) {
        return ResponHelper.ok(usersService.getUsers(Id));
    }

    @PutMapping(path = "/{Id}", consumes = "multipart/form-data")
    public CommonResponse<Users> editUsers(@PathVariable("Id") Long Id, Users users, @RequestPart("file") MultipartFile multipartFile) {
        return ResponHelper.ok(usersService.editUsers(Id,modelMapper.map(users, Users.class), multipartFile));}

    @DeleteMapping("/{Id}")
    public void deleteUsersById(@PathVariable("Id") Long Id) {
        usersService.deleteUsersById(Id);
    }

}
