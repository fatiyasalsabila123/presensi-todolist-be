package com.PresensiToDoList.PresensiToDoList.exception;

import com.PresensiToDoList.PresensiToDoList.response.ResponHelper;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

//alur ketiga pembuatan exception

public class GlobalException {

    @ExceptionHandler(ChangeSetPersister.NotFoundException.class)
    public ResponseEntity<?> notFound(ChangeSetPersister.NotFoundException notFoundException){
        return ResponHelper.error(notFoundException.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InternalErrorException.class)
    public ResponseEntity<?> internalError(InternalErrorException InternalErrorExpection){
        return ResponHelper.error(InternalErrorExpection.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
