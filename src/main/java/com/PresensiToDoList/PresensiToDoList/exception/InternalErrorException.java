package com.PresensiToDoList.PresensiToDoList.exception;

//alur kedua pembuatan exception

public class InternalErrorException extends RuntimeException{
    public InternalErrorException(String message) {
        super(message);
    }
}
