package com.PresensiToDoList.PresensiToDoList.exception;

//alur pertama pembuatan exception

public class NotFoundException extends RuntimeException{

    public NotFoundException(String message) {
        super(message);
    }
}
