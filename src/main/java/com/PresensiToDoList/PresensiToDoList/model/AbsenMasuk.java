package com.PresensiToDoList.PresensiToDoList.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

//Alur kedua pembuatan absen masuk
@Entity
@Table(name = "absen_masuk")
public class AbsenMasuk {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @JsonFormat(pattern = "yyy-MMM-dd")
    @CreationTimestamp
    @Column(name = "tanggal", updatable = false)
    private Date tanggal;

    @JsonFormat(pattern = "HH:mm:ss")
//    @CreationTimestamp
    @Column(name = "waktu", updatable = false)
    private Date waktu;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users usersId;

    public AbsenMasuk(Date tanggal, Date waktu, Users usersId) {
        this.tanggal = tanggal;
        this.waktu = waktu;
        this.usersId = usersId;
    }

    //    @JsonFormat(pattern =  "yyy-MMM-ddd HH:mm:ss")
//    @Column(name = "end_date", updatable = false)
//    private Date andDate;

//    @ManyToOne
//    @JoinColumn(name = "passanger_type_id")
//    private PassagerType passagerType;

    public AbsenMasuk() {
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Date getWaktu() {
        return waktu;
    }

    public void setWaktu(Date waktu) {
        this.waktu = waktu;
    }

//    public PassagerType getPassagerType() {
//        return passagerType;
//    }
//
//    public void setPassagerType(PassagerType passagerType) {
//        this.passagerType = passagerType;
//    }

//    public Date getAndDate() {
//        return andDate;
//    }
//
//    public void setAndDate(Date andDate) {
//        this.andDate = andDate;
//    }


    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }

    @Override
    public String toString() {
        return "AbsenMasuk{" +
                "Id=" + Id +
                ", tanggal=" + tanggal +
                ", waktu=" + waktu +
                ", usersId=" + usersId +
                '}';
    }
}
