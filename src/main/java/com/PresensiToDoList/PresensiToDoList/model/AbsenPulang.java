package com.PresensiToDoList.PresensiToDoList.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

//alur kedua pembuatan absenPulang
@Entity
@Table(name = "absen_pulang")
public class AbsenPulang {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @JsonFormat(pattern = "yyy-MMM-dd")
    @CreationTimestamp
    @Column(name = "tanggal", updatable = false)
    private Date tanggal;

    @JsonFormat(pattern =  "HH:mm:ss")
//    @CreationTimestamp
    @Column(name = "waktu_pulang", updatable = false)
    private Date waktu;

    @Column(name = "status")
    private String status;

    @ManyToOne
    @JoinColumn(name = "users_id")
    private Users usersId;

    public AbsenPulang() {
    }

    public AbsenPulang(Date tanggal, Date waktu, String status, Users usersId) {
        this.tanggal = tanggal;
        this.waktu = waktu;
        this.status = status;
        this.usersId = usersId;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Date getWaktu() {
        return waktu;
    }

    public void setWaktu(Date waktu) {
        this.waktu = waktu;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }

    @Override
    public String toString() {
        return "AbsenPulang{" +
                "Id=" + Id +
                ", tanggal=" + tanggal +
                ", waktu=" + waktu +
                ", status='" + status + '\'' +
                ", usersId=" + usersId +
                '}';
    }

}
