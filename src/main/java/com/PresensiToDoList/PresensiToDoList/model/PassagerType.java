package com.PresensiToDoList.PresensiToDoList.model;

import javax.persistence.*;

@Entity
@Table(name = "passager_type")
public class PassagerType {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "name")
    private String name;

    public PassagerType() {
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "PassagerType{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                '}';
    }
}
