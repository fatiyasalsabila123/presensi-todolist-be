package com.PresensiToDoList.PresensiToDoList.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

//Alur kedua pembuatan Presensi

@Entity
@Table(name = "presensi")
public class Presensi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "nama")
    private String nama;

    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    @CreationTimestamp
    @Column(name = "tanggal")
    private Date tanggal;

    @Column(name = "gender")
    private String gender;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users usersId;

    public Presensi() {
    }

    public Presensi(String nama, Date tanggal, String gender, Users usersId) {
        this.nama = nama;
        this.tanggal = tanggal;
        this.gender = gender;
        this.usersId = usersId;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }
}
