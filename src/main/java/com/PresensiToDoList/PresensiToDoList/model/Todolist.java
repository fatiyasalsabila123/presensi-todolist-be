package com.PresensiToDoList.PresensiToDoList.model;

import javax.persistence.*;

//Alur kedua pembuatan Todolist
@Entity
@Table(name = "todolist")
public class Todolist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "note")
    private String note;

    @Column(name = "checked")
    private String check;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users usersId;

    public Todolist(String note, String check, Users usersId) {
        this.note = note;
        this.check = check;
        this.usersId = usersId;
    }

    public Todolist() {
    }

    public Todolist(String note) {
        this.note = note;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }
}
