package com.PresensiToDoList.PresensiToDoList.model;

import com.PresensiToDoList.PresensiToDoList.enumated.UserSType;

import javax.persistence.*;
import java.util.List;

//Alur kedua pembuatan login dan register
@Entity
@Table(name = "users")
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "username")
    private String username;

    @Lob
    @Column(name= "alamat")
    private String alamat;

    @Column(name = "nohp")
    private Long nohp;

    @Lob
    @Column(name = "profile")
    private String profile;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private UserSType role;

    public Users() {
    }

    public Users(String email, String password, String username, UserSType role) {
        this.email = email;
        this.password = password;
        this.username = username;
//        this.alamat = alamat;
//        this.nohp = nohp;
//        this.profile = profile;
        this.role = role;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserSType getRole() {
        return role;
    }

    public void setRole(UserSType role) {
        this.role = role;
    }

//    public List<Todolist> getTodolists() {
//        return todolists;
//    }
//
//    public void setTodolists(List<Todolist> todolists) {
//        this.todolists = todolists;
//    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public Long getNohp() {
        return nohp;
    }

    public void setNohp(Long nohp) {
        this.nohp = nohp;
    }

    @Override
    public String toString() {
        return "Users{" +
                "Id=" + Id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", username='" + username + '\'' +
                ", alamat='" + alamat + '\'' +
                ", nohp=" + nohp +
                ", profile='" + profile + '\'' +
                ", role=" + role +
                '}';
    }
}
