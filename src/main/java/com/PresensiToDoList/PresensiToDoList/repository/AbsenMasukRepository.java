package com.PresensiToDoList.PresensiToDoList.repository;

import com.PresensiToDoList.PresensiToDoList.model.AbsenMasuk;
import com.PresensiToDoList.PresensiToDoList.model.Presensi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

//alur ketiga pembuatan absen pulang
public interface AbsenMasukRepository extends JpaRepository<AbsenMasuk, Long> {
    @Query(value = "SELECT * FROM absen_masuk  WHERE user_id = ?1", nativeQuery = true)
    List<AbsenMasuk> getAllAbsenMasuk(Long usersId );
}
