package com.PresensiToDoList.PresensiToDoList.repository;

import com.PresensiToDoList.PresensiToDoList.model.AbsenPulang;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

//alur ketiga pembuatan absen pulang
public interface AbsenPulangRepository extends JpaRepository<AbsenPulang, Long> {

    @Query(value = "SELECT * FROM absen_pulang  WHERE users_id = ?1", nativeQuery = true)
    List<AbsenPulang> getAll(Long usersId );

}
