package com.PresensiToDoList.PresensiToDoList.repository;

import com.PresensiToDoList.PresensiToDoList.model.PassagerType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassagerTypeRepository extends JpaRepository<PassagerType, Long> {
}
