package com.PresensiToDoList.PresensiToDoList.repository;

import com.PresensiToDoList.PresensiToDoList.model.Presensi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

//alur ketiga pembuatan presensi
@Repository
public interface PresensiRepository extends JpaRepository<Presensi, Long> {

    @Query(value = "SELECT * FROM presensi  WHERE user_id = ?1", nativeQuery = true)
    List<Presensi> getAll(Long usersId );


//    @Query(value = "SELECT * FROM presensi  WHERE nama LIKE CONCAT('%', ?1, '%')", nativeQuery = true)
//    Page<Presensi> searchPresensi(String query, Pageable pageable);

}
