package com.PresensiToDoList.PresensiToDoList.repository;

import com.PresensiToDoList.PresensiToDoList.model.Todolist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

//alur ketiga pembuatan todolist
@Repository
public interface TodolistRepository extends JpaRepository<Todolist, Long> {
//    List<Todolist> findByIdUser(Users users);
//

    @Query(value = "SELECT * FROM todolist  WHERE user_id = ?1", nativeQuery = true)
    List<Todolist> getAllTodolist(Long usersId );
}
