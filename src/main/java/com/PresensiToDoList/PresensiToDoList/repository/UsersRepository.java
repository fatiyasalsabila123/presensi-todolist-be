package com.PresensiToDoList.PresensiToDoList.repository;

import com.PresensiToDoList.PresensiToDoList.enumated.UserSType;
import com.PresensiToDoList.PresensiToDoList.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//alur keempat pembuatan login/register
@Repository
public interface UsersRepository extends JpaRepository<Users, Long> {

    Optional<Users> findByEmail(String email);

//    Users findByEmail(String email, String password, UserSType role);
}
