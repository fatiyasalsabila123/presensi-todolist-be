package com.PresensiToDoList.PresensiToDoList.response;

//alur pertama pembuatan response untuk login/register todolist, presensi, absen pulang dan absen masuk

public class CommonResponse <T>{

    private String message;

    private String status;

    private T data;

    public CommonResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
