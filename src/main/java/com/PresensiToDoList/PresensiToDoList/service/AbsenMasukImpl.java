package com.PresensiToDoList.PresensiToDoList.service;

import com.PresensiToDoList.PresensiToDoList.DTO.AbsenMasukDto;
import com.PresensiToDoList.PresensiToDoList.exception.InternalErrorException;
import com.PresensiToDoList.PresensiToDoList.exception.NotFoundException;
import com.PresensiToDoList.PresensiToDoList.model.AbsenMasuk;
import com.PresensiToDoList.PresensiToDoList.model.Users;
import com.PresensiToDoList.PresensiToDoList.repository.AbsenMasukRepository;
import com.PresensiToDoList.PresensiToDoList.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class AbsenMasukImpl implements MasukAbsenService{

    private static final int HOUR = 3600 * 1000;

    @Autowired
    AbsenMasukRepository absenMasukRepository;

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    UsersService usersService;

//    @Autowired
//    PassagerTypeRepository passagerTypeRepository;


//    @Transactional(readOnly = true)
//    @Override
//    public List<AbsenMasuk> getAllAbsenMasuk(Long usersId) {
//        return absenMasukRepository.findAll(usersId);
//    }


    @Override
    public List<AbsenMasuk> getAllAbsenMasuk(Long userId) {
        return absenMasukRepository.getAllAbsenMasuk(userId);
    }

    @Override
    public AbsenMasuk addAbsenMasuk(AbsenMasukDto absenMasukDto) {
        Users users= usersService.getUsers(absenMasukDto.getUsersId());
        AbsenMasuk absenMasuk1 = new AbsenMasuk();
        absenMasuk1.setWaktu(new Date(new Date().getTime() + 7 * HOUR));
        absenMasuk1.setUsersId(users);
        return absenMasukRepository.save(absenMasuk1);
    }

    @Override
    public AbsenMasuk getAbsenMasuk(Long Id) {
        var absenMasuk = absenMasukRepository.findById(Id).orElseThrow(() -> new NotFoundException(("Id Tidak Di Temukan")));
        try {
            return absenMasukRepository.save(absenMasuk);
        }catch (Exception e) {
            throw new InternalErrorException("Kesalahan Memunculkan Data");
        }
    }

    @Transactional
    @Override
    public AbsenMasuk editAbsenMasauk(Long Id, AbsenMasuk absenMasuk) {
        AbsenMasuk update = absenMasukRepository.findById(Id).orElseThrow (() -> new NotFoundException("PAssager Type Id Not Found"));
//        PassagerType passagerType = passagerTypeRepository.findById(passagerTypeId).orElseThrow(() -> new NotFoundException("Passager Type Id Not Found"));
        update.setTanggal(absenMasuk.getTanggal());
        update.setWaktu(absenMasuk.getWaktu());
        return absenMasukRepository.save(update);
    }
}
