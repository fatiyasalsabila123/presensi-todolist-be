package com.PresensiToDoList.PresensiToDoList.service;

import com.PresensiToDoList.PresensiToDoList.DTO.AbsenPulangDtro;
import com.PresensiToDoList.PresensiToDoList.exception.InternalErrorException;
import com.PresensiToDoList.PresensiToDoList.exception.NotFoundException;
import com.PresensiToDoList.PresensiToDoList.model.AbsenPulang;
import com.PresensiToDoList.PresensiToDoList.model.Users;
import com.PresensiToDoList.PresensiToDoList.repository.AbsenPulangRepository;
import com.PresensiToDoList.PresensiToDoList.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class AbsenPulangImpl implements AbsenPulangService{

    private static final int HOUR = 3600 * 1000;

    @Autowired
    AbsenPulangRepository absenPulangRepository;

    @Autowired
    UsersService usersService;

//    @Transactional(readOnly = true)
//    @Override
//    public List<AbsenPulang> getAllAbsenPulang() {
//        return absenPulangRepository.findAll();
//    }

    @Override
    public List<AbsenPulang> getAll(Long usersId) {
//        Pageable pageable = PageRequest.of(page, 3);
        return absenPulangRepository.getAll(usersId);
    }



    @Override
    public AbsenPulang addAbsenPulang(AbsenPulangDtro absenPulangDtro) {

        Users users= usersService.getUsers(absenPulangDtro.getUsersId());
        AbsenPulang absenPulang1 = new AbsenPulang();
        absenPulang1.setStatus(absenPulangDtro.getStatus());
        absenPulang1.setWaktu(new Date(new Date().getTime() + 7 * HOUR));
        absenPulang1.setUsersId(users);
        return absenPulangRepository.save(absenPulang1);
    }

    @Override
    public AbsenPulang getAbsenPulang(Long Id) {
        var absenPulang = absenPulangRepository.findById(Id).orElseThrow(() -> new NotFoundException(("Id Tidak Di Temukan")));
        try {
            return absenPulangRepository.save(absenPulang);
        }catch (Exception e) {
            throw new InternalErrorException("Kesalahan Memunculkan Data");
        }
    }

    @Transactional
    @Override
    public AbsenPulang editAbsenPulang(Long Id, AbsenPulang absenPulang) {
        AbsenPulang update = absenPulangRepository.findById(Id).orElseThrow (() -> new NotFoundException("PAssager Type Id Not Found"));
//        PassagerType passagerType = passagerTypeRepository.findById(passagerTypeId).orElseThrow(() -> new NotFoundException("Passager Type Id Not Found"));
        update.setStatus(absenPulang.getStatus());
//        update.setTanggal(absenPulang.getTanggal());
//        update.setWaktu(absenPulang.getWaktu());
        return absenPulangRepository.save(update);
    }
}
