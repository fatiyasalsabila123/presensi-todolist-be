package com.PresensiToDoList.PresensiToDoList.service;

import com.PresensiToDoList.PresensiToDoList.DTO.AbsenPulangDtro;
import com.PresensiToDoList.PresensiToDoList.model.AbsenPulang;
import com.PresensiToDoList.PresensiToDoList.model.Presensi;

import java.util.List;

//alur kelima pembuatan absen pulang

public interface AbsenPulangService {

//    List<AbsenPulang> getAllAbsenPulang();

    List<AbsenPulang> getAll(Long usersId);

    AbsenPulang addAbsenPulang( AbsenPulangDtro absenPulang);

    AbsenPulang getAbsenPulang(Long Id);

    AbsenPulang editAbsenPulang(Long Id, AbsenPulang absenPulang);

}
