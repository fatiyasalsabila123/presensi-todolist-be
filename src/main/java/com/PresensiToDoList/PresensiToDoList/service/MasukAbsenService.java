package com.PresensiToDoList.PresensiToDoList.service;

import com.PresensiToDoList.PresensiToDoList.DTO.AbsenMasukDto;
import com.PresensiToDoList.PresensiToDoList.model.AbsenMasuk;
import com.PresensiToDoList.PresensiToDoList.model.Presensi;

import java.util.List;

//alur kelima pembuatan absen masuk


public interface MasukAbsenService {

//    List<AbsenMasuk> getAllAbsenMasuk();

//    List<AbsenMasuk> getAllAbsenMasuk (Long usersId);

    List<AbsenMasuk> getAllAbsenMasuk (Long usersId);

    AbsenMasuk addAbsenMasuk( AbsenMasukDto absenMasuk);

    AbsenMasuk getAbsenMasuk(Long Id);

    AbsenMasuk editAbsenMasauk(Long Id, AbsenMasuk absenMasuk);


}
