package com.PresensiToDoList.PresensiToDoList.service;

import com.PresensiToDoList.PresensiToDoList.DTO.PresensiDto;
import com.PresensiToDoList.PresensiToDoList.DTO.UsersDTO;
import com.PresensiToDoList.PresensiToDoList.exception.NotFoundException;
import com.PresensiToDoList.PresensiToDoList.model.Presensi;
import com.PresensiToDoList.PresensiToDoList.repository.PresensiRepository;
import com.PresensiToDoList.PresensiToDoList.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class PresensiServiceimpl implements PresensitService{

    @Autowired
    PresensiRepository presensiRepository;
    @Autowired
    UsersRepository usersRepository;

//    @Override
//    public List<Presensi> getAll() {
//        return presensiRepository.findAll();
//    }

//    @Override
//    public Page<Presensi> getAll(int page, String query) {
//        Pageable pageable = PageRequest.of(page, 3);
//        return presensiRepository.searchPresensi(query, pageable);
//    }

    public List<Presensi> getAll(Long usersId) {
//        Pageable pageable = PageRequest.of(page, 3);
        return presensiRepository.getAll( usersId);
    }

    private static final Integer hour = 3600 * 1000;

    @Override
    public Presensi addPresensi(PresensiDto presensi) {
        Presensi presensi1 = new Presensi();
        presensi1.setGender(presensi.getGender());
        presensi1.setNama(presensi.getNama());
        presensi1.setTanggal(new Date(new Date().getTime() + 7 * hour));
//        presensi1.setUsersId(presensiRepository.findById(presensi.getUsersId()).orElseThrow(() -> new NotFoundException("User id tidak ditemukan")));
//        presensi1.setNama(presensi.getUsername());
        presensi1.setUsersId(usersRepository.findById(presensi.getUsersId()).orElseThrow(() -> new NotFoundException("Id not found")));
//        presensi1.setGender(presensi.getAlamat());
        return presensiRepository.save(presensi1);
    }

    @Override
    public Presensi getPresensi(Long Id) {
        return presensiRepository.findById(Id).orElseThrow(() -> new NotFoundException(("Id Tidak Ditemukan")));
    }

    @Override
    public Presensi EditPresensi (Long Id, Presensi presensi) {
        Presensi presensi1 = presensiRepository.findById(Id).orElseThrow(() -> new NotFoundException(("tidak ditemukan")));
        presensi1.setNama(presensi.getNama());
        presensi1.setGender(presensi.getGender());
        presensi1.setTanggal(presensi.getTanggal());
        return presensiRepository.save(presensi1);
    }

    @Override
    public void deletePresensiById(Long Id) {
        presensiRepository.deleteById(Id);
    }
}
