package com.PresensiToDoList.PresensiToDoList.service;

import com.PresensiToDoList.PresensiToDoList.DTO.PresensiDto;
import com.PresensiToDoList.PresensiToDoList.DTO.UsersDTO;
import com.PresensiToDoList.PresensiToDoList.model.Presensi;
import org.springframework.data.domain.Page;

import java.util.List;
//alur kelima pembuatan presensi
public interface PresensitService {

//    List<Presensi> getAll ();

    List<Presensi> getAll (Long usersId);



//    Page<Presensi> findAll (Long user_id, int page);

    Presensi addPresensi (PresensiDto presensi);

    Presensi getPresensi(Long Id);

    Presensi EditPresensi(Long Id, Presensi presensi);

    void deletePresensiById(Long Id);
}
