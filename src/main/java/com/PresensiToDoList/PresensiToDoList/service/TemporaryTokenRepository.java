package com.PresensiToDoList.PresensiToDoList.service;

import com.PresensiToDoList.PresensiToDoList.model.TemporaryToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//alur ke dua membuat temporary token repository

@Repository
public interface TemporaryTokenRepository extends JpaRepository<TemporaryToken, Long> {

    Optional<TemporaryToken> findByToken(String Token);

    Optional<TemporaryToken> findByUserId(Long userId);

}
