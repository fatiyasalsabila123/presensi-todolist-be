package com.PresensiToDoList.PresensiToDoList.service;
import com.PresensiToDoList.PresensiToDoList.DTO.ToDoListDto;
import com.PresensiToDoList.PresensiToDoList.model.Presensi;
import com.PresensiToDoList.PresensiToDoList.model.Todolist;

import java.util.List;

//alur kelima pembuatan todolist

public interface TodolistService {

//    List<Todolist> getAllTodolist ();

    List<Todolist> getAllTodolist (Long usersId);

    Todolist addTodolist (ToDoListDto todolist);

    Todolist getTodolist (Long Id);

    Todolist EditTodolist (Long Id, Todolist todolist);

    Todolist checklist(Long Id);

    void deleteTodolist(Long Id);
}
