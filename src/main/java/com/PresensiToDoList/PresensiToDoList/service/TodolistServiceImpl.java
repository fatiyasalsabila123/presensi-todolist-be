package com.PresensiToDoList.PresensiToDoList.service;

import com.PresensiToDoList.PresensiToDoList.DTO.PresensiDto;
import com.PresensiToDoList.PresensiToDoList.DTO.ToDoListDto;
import com.PresensiToDoList.PresensiToDoList.exception.NotFoundException;
import com.PresensiToDoList.PresensiToDoList.model.Presensi;
import com.PresensiToDoList.PresensiToDoList.model.Todolist;
import com.PresensiToDoList.PresensiToDoList.repository.TodolistRepository;
import com.PresensiToDoList.PresensiToDoList.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class TodolistServiceImpl implements TodolistService {

    @Autowired
    TodolistRepository todolistRepository;

    @Autowired
    UsersRepository usersRepository;

//    @Override
//    public List<Todolist> getAllTodolist() {
//        return todolistRepository.findAll();
//    }

    public List<Todolist> getAllTodolist(Long usersId) {
//        Pageable pageable = PageRequest.of(page, 3);
        return todolistRepository.getAllTodolist(usersId);
    }

    @Override
    public Todolist addTodolist(ToDoListDto toDoListDto) {
        Todolist todolist1 = new Todolist();
        todolist1.setNote(toDoListDto.getNote());
//        todolist1.setCheck(false);
        todolist1.setUsersId(usersRepository.findById(toDoListDto.getUsersId()).orElseThrow(() -> new NotFoundException("Id not found")));
        return todolistRepository.save(todolist1);
    }

    @Override
    public Todolist getTodolist(Long Id) {
        return todolistRepository.findById(Id).orElseThrow(() -> new NotFoundException(("Id tidak di temukan")));
    }

    @Override
    public Todolist EditTodolist(Long Id, Todolist todolist) {
        Todolist todolist1 = todolistRepository.findById(Id).orElseThrow(() -> new NotFoundException(("tidak ditemukan")));
        todolist1.setNote(todolist.getNote());
        return todolistRepository.save(todolist1);
    }


    @Transactional
    @Override
    public Todolist checklist(Long Id) {
        Todolist todolist1 = todolistRepository.findById(Id).orElseThrow(() -> new NotFoundException(("tidak ditemukan")));
        todolist1.setCheck("Selesai");
        return todolist1;
    }


    @Override
    public void deleteTodolist(Long Id) {
        todolistRepository.deleteById(Id);
    }
}
