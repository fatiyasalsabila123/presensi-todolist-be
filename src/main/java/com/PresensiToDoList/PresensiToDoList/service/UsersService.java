package com.PresensiToDoList.PresensiToDoList.service;

import com.PresensiToDoList.PresensiToDoList.DTO.LoginDto;
import com.PresensiToDoList.PresensiToDoList.DTO.UsersDTO;
import com.PresensiToDoList.PresensiToDoList.model.Users;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

//alur ke enam pembuatan login/register

public interface UsersService {

    Map<String, Object> login (LoginDto loginDto);

    List<Users> getAllUsers();

    Users addUsers(UsersDTO usersDTO);

    @Transactional
    Users editUsers(Long Id, Users users, MultipartFile multipartFile);

    Users getUsers(Long Id);

    Map<String, Boolean> deleteUsersById(Long Id);

}
