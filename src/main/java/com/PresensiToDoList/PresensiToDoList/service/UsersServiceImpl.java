package com.PresensiToDoList.PresensiToDoList.service;

import com.PresensiToDoList.PresensiToDoList.DTO.LoginDto;
import com.PresensiToDoList.PresensiToDoList.DTO.UsersDTO;
import com.PresensiToDoList.PresensiToDoList.JWT.JWTProvider;
import com.PresensiToDoList.PresensiToDoList.enumated.UserSType;
import com.PresensiToDoList.PresensiToDoList.exception.InternalErrorException;
import com.PresensiToDoList.PresensiToDoList.exception.NotFoundException;
import com.PresensiToDoList.PresensiToDoList.model.Users;
import com.PresensiToDoList.PresensiToDoList.repository.UsersRepository;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//alur ke delapan pembuatan login/register

@Service
public class UsersServiceImpl implements UsersService{



    private static final String DOWNLOAD_URL= "https://firebasestorage.googleapis.com/v0/b/profile-user-cbbd7.appspot.com/o/%s?alt=media";

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JWTProvider jwtProvider;

    @Autowired
    PasswordEncoder passwordEncoder;

    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        }catch (BadCredentialsException e) {
            throw new InternalErrorException("Email Or Pasword Not Found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }

    @Override
    public Map<String, Object> login(LoginDto loginDto) {
        String token = authories(loginDto.getEmail(), loginDto.getPassword());
        Users users1 = usersRepository.findByEmail(loginDto.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "60 menit");
        response.put("users", users1);
        return response;
    }

    @Override
    public List<Users> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public Users addUsers(UsersDTO usersDTO) {
        String email = usersDTO.getEmail();
        Users users1 = new Users(usersDTO.getEmail(), usersDTO.getPassword(), usersDTO.getUsername(), UserSType.USER);
        users1.setPassword(passwordEncoder.encode(usersDTO.getPassword()));
        var validasi = usersRepository.findByEmail(email);
        if (validasi.isPresent()) {
            throw new InternalErrorException("Email Already Axist");
        }
        return usersRepository.save(users1);
    }

    private String imageConverter(MultipartFile multipartFile){
        try {
            String fileName = getExtenions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        }catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error Upload File");
        }
    }

    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)){
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String convertToBase64Url(MultipartFile file) {
        String url = "";
        try{
            byte[] byte1 = Base64.encodeBase64(file.getBytes());
            String result = new String(byte1);
            url = "data:" + file.getContentType() + ";base64," + result;
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            return  url;
        }

    }

    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("profile-user-cbbd7.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    private String getExtenions(String fileName) {
        return fileName.split("\\.")[0];
    }

    @Transactional
    @Override
    public Users editUsers(Long Id, Users users, MultipartFile multipartFile) {
        String profile = convertToBase64Url(multipartFile);
        users.setPassword(passwordEncoder.encode(users.getPassword()));
        Users update = usersRepository.findById(Id).orElseThrow(() -> new NotFoundException("Tidak Ada"));
        update.setEmail(users.getEmail());
        update.setPassword(users.getPassword());
        update.setUsername(users.getUsername());
        update.setAlamat(users.getAlamat());
        update.setNohp(users.getNohp());
        update.setProfile(profile);
        return update;
    }

    @Override
    public Users getUsers(Long Id) {
        return usersRepository.findById(Id).orElseThrow(() -> new NotFoundException(("id tidak di temukan")));
    }

    @Override
    public Map<String, Boolean> deleteUsersById(Long Id) {
        try {
            usersRepository.deleteById(Id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
